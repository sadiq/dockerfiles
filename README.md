# Docker images for GTK application developers


Supported tags:

1. `pksadiq/emacs:alpine-amd64`:
   * Base image: Alpine Edge
   * Emacs 29.1 + htmlize + buttercup

1. `pksadiq/meson:alpine-amd64`:
   * Base image: Alpine Edge
   * gcc, ccache, mold, meson, [μtest][mutest]

1. `pksadiq/gtk:alpine-amd64`:
   * Base image: Alpine Edge
   * GTK3/GTK4 Development for GNU/Linux

1. `pksadiq/gtk:debian-amd64`:
   * Base image: Debian sid
   * GTK3/GTK4 Development for GNU/Linux

1. `pksadiq/gtk:fedora-amd64`:
   * Base image: Fedora 39
   * GTK3/GTK4 Development for GNU/Linux

1. `pksadiq/gtk:fedora-mingw32`:
   * Base image: Fedora 39
   * GTK3 Development for Windows (using MinGW)


Please see [My GTemplate][my-gtemplate] project for a feature rich
GTK3/GTK4 Template released into Public Domain (CC0).

[mutest]: https://github.com/ebassi/mutest
[my-gtemplate]: https://www.sadiqpk.org/projects/my-gtemplate.html
