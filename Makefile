all:
	$(info Add appropriate command to run)

build-all: build-alpine-emacs-amd64 build-alpine-meson-amd64 build-alpine-gtk-amd64 \
	build-debian-gtk-amd64 build-fedora-gtk-amd64 build-fedora-gtk-mingw64

push-all: push-alpine-emacs-amd64 push-alpine-meson-amd64 push-alpine-gtk-amd64 \
	push-debian-gtk-amd64 push-fedora-gtk-amd64 push-fedora-gtk-mingw64

build-alpine-emacs-amd64:
	docker build -t pksadiq/emacs:alpine-amd64 -f alpine-emacs .

build-alpine-meson-amd64:
	docker build -t pksadiq/meson:alpine-amd64 -f alpine-meson .

build-alpine-gtk-amd64:
	docker build -t pksadiq/gtk:alpine-amd64 -f alpine-gtk .

build-debian-gtk-amd64:
	docker build -t pksadiq/gtk:debian-amd64 -f debian-gtk .

build-fedora-gtk-amd64:
	docker build -t pksadiq/gtk:fedora-amd64 -f fedora-gtk .

build-fedora-gtk-mingw64:
	docker build -t pksadiq/gtk:fedora-mingw64 -f fedora-mingw-gtk .

push-alpine-emacs-amd64:
	docker push pksadiq/emacs:alpine-amd64

push-alpine-meson-amd64:
	docker push pksadiq/meson:alpine-amd64

push-alpine-gtk-amd64:
	docker push pksadiq/gtk:alpine-amd64

push-debian-gtk-amd64:
	docker push pksadiq/gtk:debian-amd64

push-fedora-gtk-amd64:
	docker push pksadiq/gtk:fedora-amd64

push-fedora-gtk-mingw64:
	docker push pksadiq/gtk:fedora-mingw64

clean:
	docker rmi pksadiq/emacs:alpine-amd64 pksadiq/meson:alpine-meson-amd64 \
	pksadiq/gtk:alpine-amd64 pksadiq/gtk:debian-amd64 pksadiq/gtk:fedora-amd64 \
	pksadiq/gtk:fedora-mingw64 alpine:edge debian:sid fedora:38
