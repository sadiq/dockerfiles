(require 'package)

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

(setq package-selected-packages
      '(htmlize buttercup))

(package-install-selected-packages t)
